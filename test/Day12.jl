using Test

using Aoc2020
using Aoc2020.Day12:
    parseinstruction, moveship, navigate_direct, movewaypoint, navigate_waypoint

@testset "Day12" begin
    @test parseinstruction("N10") == ('N', 10)
    @test_throws ArgumentError parseinstruction("A12")
    @test_throws ArgumentError parseinstruction("L12")

    @test moveship((0, 0), 0, ('N', 5)) == ((0, 5), 0)
    @test moveship((0, 0), 0, ('S', 7)) == ((0, -7), 0)
    @test moveship((0, 0), 0, ('E', 10)) == ((10, 0), 0)
    @test moveship((0, 0), 0, ('W', 2)) == ((-2, 0), 0)
    @test moveship((0, 0), 0, ('L', 90)) == ((0, 0), 90)
    @test moveship((0, 0), 0, ('R', 90)) == ((0, 0), 270)
    @test moveship((0, 0), 0, ('F', 4)) == ((4, 0), 0)
    @test moveship((0, 0), 90, ('F', 4)) == ((0, 4), 90)
    @test moveship((0, 0), 180, ('F', 4)) == ((-4, 0), 180)
    @test moveship((0, 0), 270, ('F', 4)) == ((0, -4), 270)

    @test movewaypoint((0, 0), ('N', 5)) == (0, 5)
    @test movewaypoint((0, 0), ('S', 5)) == (0, -5)
    @test movewaypoint((0, 0), ('E', 5)) == (5, 0)
    @test movewaypoint((0, 0), ('W', 5)) == (-5, 0)
    @test movewaypoint((10, 1), ('L', 90)) == (-1, 10)
    @test movewaypoint((10, 1), ('L', 180)) == (-10, -1)
    @test movewaypoint((10, 1), ('L', 270)) == (1, -10)
    @test movewaypoint((10, 1), ('R', 90)) == (1, -10)

    instructions = parseinstruction.(splitlines("""
        F10
        N3
        F7
        R90
        F11
        """))
    @test navigate_direct(instructions) == ((17, -8), 270)
    @test navigate_waypoint(instructions) == ((214, -72), (4, -10))
end
