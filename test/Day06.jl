module TestDay06

using Test

using Aoc2020.Day06: parseanswers, groupunique, groupall, run

input = """
abc\n
a\nb\nc\n
ab\nac\n
a\na\na\na\n
b\n
"""

@test parseanswers(input) == [
    ["abc"], ["a", "b", "c"], ["ab", "ac"], ["a", "a", "a", "a"], ["b"]
]

@test groupunique(["abc"]) == Set("abc")
@test groupunique(["a", "b", "c"]) == Set("abc")
@test groupunique(["ab", "ac"]) == Set("abc")
@test groupunique(["a", "a", "a", "a"]) == Set('a')

@test groupall(["abc"]) == Set("abc")
@test groupall(["a", "b", "c"]) == Set()
@test groupall(["ab", "ac"]) == Set('a')
@test groupall(["a", "a", "a", "a"]) == Set('a')

# @test isnothing(run())

end  # module TestDay06
