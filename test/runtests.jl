using Test

@testset "Aoc2020" begin
    @testset "Day01" begin
        using Aoc2020.Day01: findcouple, findtuple, subsets_mat, run

        # Part 1
        @test findcouple([2, 3], 5) == (2, 3)
        @test findcouple([2, 3], 6) === nothing
        @test findcouple([1721, 979, 366, 299, 675, 1456], 2020) == (1721, 299)
        @test_throws ArgumentError findcouple(Vector{Int}(), 2)
        @test_throws ArgumentError findcouple([1], 2)

        # Part 2
        @test subsets_mat(0, 0) == [Int[]]
        @test subsets_mat(0, 1) == [Int[]]
        @test subsets_mat(1, 1) == [[1]]
        @test subsets_mat(1, 3) == [[1], [2], [3]]
        @test subsets_mat(2, 2) == [[1, 2]]
        @test subsets_mat(2, 3) == [[1, 2], [1, 3], [2, 3]]
        @test_throws ArgumentError subsets_mat(1, 0)

        @test findtuple([1721, 979, 366, 299, 675, 1456], 3, 2020) == (979, 366, 675)

        # @test isnothing(run())
    end

    @testset "Day02" begin
        using Aoc2020.Day02: isvalidcount, isvalidposition, parseline, PasswordPolicy, run

        @test parseline("8-9 x: xxxxxxxrk") == (PasswordPolicy('x', 8, 9), "xxxxxxxrk")
        @test parseline("9-10 l: lllllllllk") == (PasswordPolicy('l', 9, 10), "lllllllllk")
        @test_throws ArgumentError parseline("a")

        @test isvalidcount("abbbc", PasswordPolicy('b', 1, 3))
        @test !isvalidcount("abbbc", PasswordPolicy('b', 1, 2))
        @test !isvalidcount("abbbc", PasswordPolicy('b', 4, 5))

        @test isvalidposition("abcd", PasswordPolicy('c', 1, 3))
        @test isvalidposition("cbac", PasswordPolicy('c', 1, 3))
        @test !isvalidposition("cbcd", PasswordPolicy('c', 1, 3))
        @test !isvalidposition("abad", PasswordPolicy('c', 1, 3))

        # @test isnothing(run())
    end

    @testset "Day03" begin
        using Aoc2020.Day03: parsegrid, get, traverse, run

        grid = ['a' 'b' 'c'; 'd' 'e' 'f']
        @test parsegrid("abc\ndef") == grid

        @test get(grid, 1, 1) == 'a'
        @test get(grid, 1, 3) == 'c'
        @test get(grid, 1, 4) == 'a'
        @test get(grid, 2, 5) == 'e'

        @test traverse(grid, (1, 1)) == ['a', 'e']
        @test traverse(grid, (1, 2)) == ['a', 'f']
        @test traverse(grid, (2, 1)) == ['a']

        # @test isnothing(run())
    end

    @testset "Day04" begin
        using Aoc2020.Day04:
            parsepassports, hasvalidkeys, isvalidint, isvalidheight, isvalidpassport, run

        @test parsepassports("") == []
        @test parsepassports("a:b\nc:d e:f") == [Dict("a" => "b", "c" => "d", "e" => "f")]
        @test parsepassports("a:1 b:2\nc:3\n\nd:4\ne:5") == [
            Dict("a" => "1", "b" => "2", "c" => "3"), Dict("d" => "4", "e" => "5")
        ]

        @test hasvalidkeys(Dict{String,String}(), [], [])
        @test hasvalidkeys(Dict("a" => "1"), ["a"], [])
        @test hasvalidkeys(Dict("a" => "1"), [], ["a"])
        @test hasvalidkeys(Dict("a" => "1"), ["a"], ["b"])
        @test !hasvalidkeys(Dict("a" => "1"), ["b"], ["a"])
        @test !hasvalidkeys(Dict("a" => "1", "b" => "2"), ["a"], [])

        @test isvalidint("12", 0, 100)
        @test !isvalidint("12a", 0, 100)
        @test !isvalidint("12", 0, 10)
        @test !isvalidint("12", 20, 100)

        bounds = Dict("cm" => (5, 100), "in" => (10, 30))
        @test isvalidheight("100cm", bounds)
        @test isvalidheight("15in", bounds)
        @test !isvalidheight("100com", bounds)
        @test !isvalidheight("1cm", bounds)
        @test !isvalidheight("101cm", bounds)
        @test !isvalidheight("5in", bounds)
        @test !isvalidheight("32in", bounds)

        valid_passports = parsepassports("""
            pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
            hcl:#623a2f

            eyr:2029 ecl:blu cid:129 byr:1989
            iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

            hcl:#888785
            hgt:164cm byr:2001 iyr:2015 cid:88
            pid:545766238 ecl:hzl
            eyr:2022

            iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
            """)
        invalid_passports = parsepassports("""
            eyr:1972 cid:100
            hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

            iyr:2019
            hcl:#602927 eyr:1967 hgt:170cm
            ecl:grn pid:012533040 byr:1946

            hcl:dab227 iyr:2012
            ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

            hgt:59cm ecl:zzz
            eyr:2038 hcl:74454a iyr:2023
            pid:3556412378 byr:2007
            """)
        @test all(isvalidpassport.(valid_passports))
        @test all(map(!isvalidpassport, invalid_passports))

        # @test isnothing(run())
    end

    @testset "Day05" begin
        using Aoc2020.Day05: Seat, run

        s = Seat(2, 2)
        @test (s.row, s.column, s.id) == (2, 2, 18)
        @test_throws ArgumentError Seat(-1, 2)
        @test_throws ArgumentError Seat(2, -1)

        s = Seat("FBFBBFFRLR")
        @test (s.row, s.column, s.id) == (44, 5, 357)
        @test_throws ArgumentError Seat("FBFBBFFRLRA")
        @test_throws ArgumentError Seat("FBFBBFFRL")
        @test_throws KeyError Seat("FBFBBFFRLA")
        @test_throws KeyError Seat("LBFBBFFRLR")
        @test_throws KeyError Seat("FBFBBFFRLF")

        # @test isnothing(run())
    end

    test_files = filter(x -> endswith(x, ".jl") && x != "runtests.jl", readdir(@__DIR__))
    @testset "$(splitext(test_file)[1])" for test_file in test_files
        include(test_file)
    end
end
