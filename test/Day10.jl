using Test

using Aoc2020.Day10: prepare, valcount, countarrangements

@testset "Day10" begin
    @test valcount([1, 2, 2, 3]) == Dict(1 => 1, 2 => 2, 3 => 1)

    data = prepare([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
    @test valcount(diff(data)) == Dict(1 => 7, 3 => 5)
    @test countarrangements(data, 3) == 8

    data2 = prepare(
        [
            28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32,
            25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3
        ]
    )
    @test countarrangements(data2, 3) == 19208
end
