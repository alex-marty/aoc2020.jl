using Test

using Aoc2020.Day08: parsecode, runprogram, fixprogram

@testset "Day08" begin
    @test parsecode("") == []
    @test parsecode("abc 12") == [("abc", 12)]
    @test parsecode("abc 12\ndef -3") == [("abc", 12), ("def", -3)]
    
    input = """
        nop +0
        acc +1
        jmp +4
        acc +3
        jmp -3
        acc -99
        acc +1
        jmp -4
        acc +6"""
    code = parsecode(input)
    @test runprogram(code) == (1, 2, 5)
    @test fixprogram(code) == (0, 10, 8)
end
