using Test

using Aoc2020.Day09: findsum, findsumlist, runxmas

@testset "Day09" begin
    @test findsum(26, collect(1:25)) == (1, 25)
    @test findsum(49, collect(1:25)) == (24, 25)
    @test findsum(100, collect(1:25)) === nothing
    @test findsum(50, collect(1:25)) === nothing

    data = [
        35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
        576
    ]
    @test runxmas(data, 5) == 127

    @test findsumlist(127, data) == [15, 25, 47, 40]
end
