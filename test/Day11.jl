using Test

using Aoc2020
using Aoc2020.Day11:
    adjacentindices, visibleindices, updateseats, seatsimulation_adjacent,
    seatsimulation_visible

@testset "Day11" begin
    grid = parsegrid(
        """
        L.LL.LL.LL
        LLLLLLL.LL
        L.L.L..L..
        LLLL.LL.LL
        L.LL.LL.LL
        L.LLLLL.LL
        ..L.L.....
        LLLLLLLLLL
        L.LLLLLL.L
        L.LLLLL.LL
        """
    )

    @test adjacentindices(
        grid, CartesianIndex(1, 1)
    ) == CartesianIndex.([(2, 1), (1, 2), (2, 2)])
    @test adjacentindices(
        grid, CartesianIndex(3, 3)
    ) == CartesianIndex.([(2, 2), (3, 2), (4, 2), (2, 3), (4, 3), (2, 4), (3, 4), (4, 4)])
    @test adjacentindices(
        grid, CartesianIndex(5, 10)
    ) == CartesianIndex.([(4, 9), (5, 9), (6, 9), (4, 10), (6, 10)])

    g1 = parsegrid("""
        .......#.
        ...#.....
        .#.......
        .........
        ..#L....#
        ....#....
        .........
        #........
        ...#.....
        """)
    @test visibleindices(
        g1, CartesianIndex(5, 4)
    ) == CartesianIndex.([(3, 2), (5, 3), (8, 1), (2, 4), (9, 4), (1, 8), (5, 9), (6, 5)])

    g2 = parsegrid("""
        .............
        .L.L.#.#.#.#.
        .............
        """)
    @test visibleindices(g2, CartesianIndex(2, 2)) == [CartesianIndex(2, 4)]
    @test visibleindices(g2, CartesianIndex(2, 4)) == CartesianIndex.([(2, 2), (2, 6)])

    g3 = parsegrid("""
        .##.##.
        #.#.#.#
        ##...##
        ...L...
        ##...##
        #.#.#.#
        .##.##.
        """)
    @test visibleindices(g3, CartesianIndex(4, 4)) == []

    @test seatsimulation_adjacent(grid) == 37
    @test seatsimulation_visible(grid) == 26
end