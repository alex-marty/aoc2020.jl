using Test

using Aoc2020.Day07: BagContent, parsebag, bagcontainers, allparents, nchildren

@testset "Day07" begin
    @test parsebag(
        "light red bags contain 1 bright white bag, 2 muted yellow bags."
    ) == ("light red" => [(n=1, bag="bright white"), (n=2, bag="muted yellow")])
    @test parsebag(
        "bright white bags contain 1 shiny gold bag."
    ) == ("bright white" => [(n=1, bag="shiny gold")])
    @test parsebag("faded blue bags contain no other bags.") == ("faded blue" => [])
     
    bagdefs = Dict{String,Vector{BagContent}}(
        "a" => [(n=1, bag="b"), (n=2, bag="c")],
        "b" => [(n=3, bag="d")],
        "c" => [(n=2, bag="d")],
        "d" => [],
    )
    containers = bagcontainers(bagdefs)
    @test containers == Dict(
        "a" => Set(), "b" => Set(["a"]), "c" => Set(["a"]), "d" => Set(["b", "c"])
    )

    @test allparents("a", containers) == Set()
    @test allparents("b", containers) == Set(["a"])
    @test allparents("d", containers) == Set(["b", "c", "a"])

    @test nchildren("d", bagdefs) == 0
    @test nchildren("c", bagdefs) == 2
    @test nchildren("a", bagdefs) == 1*(1 + 3) + 2*(1 + 2)
end
