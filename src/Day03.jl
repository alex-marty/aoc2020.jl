module Day03

using ..Aoc2020: @daycli

Grid = Array{Char,2}

"""
    parsegrid(input)

Parse a grid definition string.
"""
function parsegrid(input::String)::Grid
    grid_data = collect.(split(input, '\n'; keepempty=false))
    return permutedims(hcat(grid_data...))
end

"""
    get(grid, x, y)

Return the content of the square at line `x` and column `y` of `grid`, wrapping the grid
around to the right.
"""
function get(grid::Grid, x::Int, y::Int)::Char
    return grid[x, mod(y - 1, size(grid)[2]) + 1]
end

"""
    traverse(grid, slope)

Return the sequence of squares traversed in `grid` when starting at position `(1, 1)` and
traversing it with slope `slope=(a, b)`, i.e. moving down `a` squares and to the right `b`
squares.
"""
function traverse(grid::Grid, slope::Tuple{Int,Int})::Vector{Char}
    x, y = 1, 1
    squares = Char[]
    while x <= size(grid)[1]
        push!(squares, get(grid, x, y))
        x += slope[1]
        y += slope[2]
    end
    return squares
end

"""
    day03(input_data)

Run day 3 puzzle, reading input data from `path`.
"""
@daycli 3 function run(input_data::String)
    grid = parsegrid(input_data)

    # Part 1
    traversal = traverse(grid, (1, 3))
    println("Part 1: $(count(x -> x == '#', traversal)) trees / $(length(traversal))")

    # Part 2
    slopes = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
    tree_counts = Dict(
        slope => count(x -> x == '#', traverse(grid, slope)) for slope in slopes
    )
    println("Part 2: tree_counts=$tree_counts, prod=$(prod(values(tree_counts)))")
end

end  # module Day03
