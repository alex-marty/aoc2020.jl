module Day02

using ..Aoc2020: @daycli

struct PasswordPolicy
    letter::Char
    min::Int
    max::Int
end

"""
    parseline(line)

Parse one line of the puzzle input data, and return the password policy and password it
represents.
"""
function parseline(line::AbstractString)::Tuple{PasswordPolicy,String}
    line_regex = r"^(?<min>\d+)-(?<max>\d+) (?<letter>[a-z]): (?<password>[a-z]+)$"
    line_match = match(line_regex, line)
    isnothing(line_match) && throw(ArgumentError("invalid password db entry \"$line\""))
    return (
        PasswordPolicy(
            only(line_match["letter"]),
            parse(Int, line_match["min"]),
            parse(Int, line_match["max"]),
        ),
        line_match["password"],
    )
end

"""
    isvalidcount(password, policy)

Indicate whether `password` matches `policy`, under the count policy: `policy.letter` must
appear between `policy.min` and `policy.max` times in `password`.
"""
function isvalidcount(password::AbstractString, policy::PasswordPolicy)::Bool
    n = count(x -> x == policy.letter, password)
    return policy.min <= n <= policy.max
end

"""
    isvalidposition(password, policy)

Indicate whether `password` matches `policy`, under the position policy: `policy.letter`
must appear at exactly one of the positions `policy.min` or `policy.max` in `password`.
Other positions are irrelevant.
"""
function isvalidposition(password::AbstractString, policy::PasswordPolicy)::Bool
    return (password[policy.min] == policy.letter) ⊻ (password[policy.max] == policy.letter)
end

"""
    day02(input_data)

Run day 2 puzzle, reading input data from `path`.
"""
@daycli 2 function run(input_data::String)
    input_data = parseline.(split(input_data, '\n'; keepempty=false))

    # Part 1
    n_valid_count = count(entry -> isvalidcount(entry[2], entry[1]), input_data)
    println("Part 1: $n_valid_count valid / $(length(input_data)) total")

    # Part 2
    n_valid_pos = count(entry -> isvalidposition(entry[2], entry[1]), input_data)
    println("Part 2: $n_valid_pos valid / $(length(input_data)) total")
end

end  # module Day02
