module Day04

using ..Aoc2020: @daycli

function parsepassports(input::AbstractString)::Vector{Dict{String,String}}
    doc_lines = split(input, "\n\n"; keepempty=false)
    docs = split.(doc_lines; keepempty=false)
    return [Dict{String,String}(split.(doc, ":"; limit=2)) for doc in docs]
end

required_fields = [
    "byr",  # (Birth Year)
    "iyr",  # (Issue Year)
    "eyr",  # (Expiration Year)
    "hgt",  # (Height)
    "hcl",  # (Hair Color)
    "ecl",  # (Eye Color)
    "pid",  # (Passport ID)
]

optional_fields = [
    "cid",  # (Country ID)
]

fields = [required_fields..., optional_fields...]

function hasvalidkeys(dict::Dict{String,String}, required::Vector, optional::Vector)::Bool
    k = keys(dict)
    return issubset(k, [required..., optional...]) && issubset(required, k)
end

function isvalidint(str::String, lower::Int, upper::Int)::Bool
    value = tryparse(Int, str)
    return !isnothing(value) && lower <= value <= upper
end

height_regex = r"^(\d+)(cm|in)$"

function isvalidheight(str::String, unit_bounds::Dict{String,Tuple{Int,Int}})::Bool
    re_match = match(height_regex, str)
    isnothing(re_match) && return false
    height = parse(Int, re_match[1])
    unit = re_match[2]
    lower, upper = unit_bounds[unit]
    return lower <= height <= upper
end

color_regex = r"^#[0-9a-f]{6}$"
eye_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
id_regex = r"^[0-9]{9}$"

function isvalidpassport(passport::Dict{String,String})::Bool
    return (
        hasvalidkeys(passport, required_fields, optional_fields)
        && isvalidint(passport["byr"], 1920, 2002)
        && isvalidint(passport["iyr"], 2010, 2020)
        && isvalidint(passport["eyr"], 2020, 2030)
        && isvalidheight(passport["hgt"], Dict("cm" => (150, 193), "in" => (59, 76)))
        && occursin(color_regex, passport["hcl"])
        && passport["ecl"] in eye_colors
        && occursin(id_regex, passport["pid"])
    )
end

@daycli 4 function run(input_data::String)
    passports = parsepassports(input_data)

    valid = [hasvalidkeys(p, required_fields, optional_fields) for p in passports]
    println("Part 1: $(sum(valid)) valid / $(length(passports))")

    println("Part 2: $(sum(isvalidpassport.(passports))) valid / $(length(passports))")
end

end  # module Day04
