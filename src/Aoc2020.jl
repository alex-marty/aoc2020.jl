module Aoc2020

using Base: @__doc__
using Base.Meta: esc

include("Cli.jl")
using .Cli

export @daycli, parsegrid, parseints, splitlines

const DATA_DIR = normpath(joinpath(@__DIR__, "../data"))

day_name(n_day::Int) = "day" * lpad(n_day, 2, '0')
default_path(n_day::Int) = joinpath(DATA_DIR, "$(day_name(n_day)).txt")

function runday(n_day::Int, callback::Function, input_path::String=default_path(n_day))
    println("Aoc2020 Day $n_day")
    println("Reading data from \"$input_path\"")
    input_data = read(input_path, String)
    return callback(input_data)
end

macro daycli(n_day::Int, funcdef::Expr)
    funcsym = Cli.funcsymbol(funcdef)
    funcname = string(funcsym)
    return quote
        @__doc__ $(esc(funcdef))
        clirun(args...) = runday($n_day, $(esc(funcsym)), args...)
        @subcommand cli $(day_name(n_day)) clirun
    end
end

cli = CommandGroup("main")

"""
    splitlines(input)

Split a string in an array of its lines. The final trailing newline is stripped if present.
"""
splitlines(input::String)::Vector{String} = split(strip(input, '\n'), '\n')

"""
    parseints(input)

Parse a string containing ints each on its own line into a vector of ints.
"""
parseints(input::String)::Vector{Int} = map(x -> parse(Int, x), splitlines(input))

"""
    parsegrid(input)

Parse a grid definition string.
"""
function parsegrid(input::String)::Matrix{Char}
    grid_data = collect.(splitlines(input))
    return permutedims(hcat(grid_data...))
end

day_files = filter(x -> startswith(x, "Day") && endswith(x, ".jl"), readdir(@__DIR__))
for day_file in day_files
    include(day_file)
end

# Run CLI if called as a script
if abspath(PROGRAM_FILE) == @__FILE__
    Cli.run(cli, ARGS)
end

end
