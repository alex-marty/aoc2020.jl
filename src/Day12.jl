module Day12

using ..Aoc2020

function parseinstruction(line::String)::Tuple{Char,Int}
    action = line[1]
    value = parse(Int, line[2:end])
    action in "NSEWLRF" || throw(ArgumentError("invalid action char '$line'"))
    if action in "LR"
        value >= 0 && value % 90 == 0 || throw(ArgumentError("invalid turn value '$line'"))
    end
    return action, value
end

angle_map = Dict(0 => 'E', 90 => 'N', 180 => 'W', 270 => 'S')
dir_map = Dict('E' => (1, 0), 'N' => (0, 1), 'W' => (-1, 0), 'S' => (0, -1))

function moveship(
    pos::Tuple{Int,Int}, angle::Int, instruction::Tuple{Char,Int}
)::Tuple{Tuple{Int,Int},Int}
    action, val = instruction
    if action == 'L'
        angle = (angle + val) % 360
        return pos, angle
    elseif action == 'R'
        angle = (angle + 360 - val) % 360
        return pos, angle
    elseif action == 'F'
        action = angle_map[angle]
    end
    return pos .+ dir_map[action] .* val, angle
end

function navigate_direct(instructions::Vector{Tuple{Char,Int}})::Tuple{Tuple{Int,Int},Int}
    return foldl((a, b) -> moveship(a..., b), instructions; init=((0, 0), 0))
end

rotation = Dict(
    90 => (x, y) -> (-y, x),
    180 => (x, y) -> (-x, -y),
    270 => (x, y) -> (y, -x),
)

function movewaypoint(pos::Tuple{Int,Int}, instruction::Tuple{Char,Int})::Tuple{Int,Int}
    action, val = instruction
    if action == 'L'
        return rotation[val % 360](pos...)
    elseif action == 'R'
        return rotation[(360 - val) % 360](pos...)
    end
    return pos .+ dir_map[action] .* val
end

function navigate_waypoint(
    instructions::Vector{Tuple{Char,Int}}
)::Tuple{Tuple{Int,Int},Tuple{Int,Int}}
    pos = (0, 0)
    wp = (10, 1)
    for i in instructions
        if i[1] == 'F'
            pos = pos .+ wp .* i[2]
        else
            wp = movewaypoint(wp, i)
        end
    end
    return pos, wp
end

@daycli 12 function run(input_data::String)
    instructions = parseinstruction.(splitlines(input_data))
    direct_pos, _ = navigate_direct(instructions)
    println("Part 1: $(sum(abs.(direct_pos))) (pos=$direct_pos)")
    waypoint_pos, _ = navigate_waypoint(instructions)
    println("Part 2: $(sum(abs.(waypoint_pos))) (pos=$waypoint_pos)")
end

end  # module Day12
