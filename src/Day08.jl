module Day08

using ..Aoc2020: @daycli

Instruction = Tuple{String,Int}

"""
    parsecode(input)

Parse the puzzle's input, returning a vector of instructions as 2-tuples of (operation,
argument).
"""
function parsecode(input::String)::Vector{Instruction}
    return map(
        x -> begin
            op, arg = split(x)
            return op, parse(Int, arg)
        end,
        split(input, '\n'; keepempty=false)
    )
end

"""
    runprogram(code)

Run the input program and return the state at the end of execution as a 3-tuple (resultcode
(termination reason), ptr (program pointer), acc (accumulator)). The program terminates
either if the end of the code (ptr == length(code) + 1) has been reached (resultcode = 0),
or if an infinite loop is encountered (resultcode = 1).
"""
function runprogram(code::Vector{Instruction})::NTuple{3,Int}
    ptr = 1
    acc = 0
    seen = Set{Int}()
    while true
        ptr == length(code) + 1 && return (0, ptr, acc)
        ptr in seen && return (1, ptr, acc)
        push!(seen, ptr)

        op, arg = code[ptr]
        if op == "acc"
            acc += arg
            ptr += 1
        elseif op == "jmp"
            ptr += arg
        elseif op == "nop"
            ptr += 1
        else
            error("invalid instruction: $((op, arg))")
        end
    end
end

"""
    fixprogram(code)

Fix the program by changing one operation in the code to avoid infinite loops and allow
reaching the end of the program. Only one operation can be change, swapping "jmp" for "nop"
or "nop" for "jmp". Return the state at the end of the program on success, or `nothing` if
the program could not be fixed.
"""
function fixprogram(code::Vector{Instruction})::Union{NTuple{3,Int},Nothing}
    opmap = Dict("jmp" => "nop", "nop" => "jmp")
    for ptr in 1:length(code)
        op, arg = code[ptr]
        newop = get(opmap, op, nothing)
        isnothing(newop) && continue
        
        newcode = copy(code)
        newcode[ptr] = (newop, arg)
        returncode, p, acc = runprogram(newcode)
        if returncode == 0
            return returncode, p, acc
        end
    end
    return nothing
end

"""
    run(input_data)

Run day 8 puzzle.
"""
@daycli 8 function run(input_data::String)
    code = parsecode(input_data)
    println("Part 1: $(runprogram(code))")
    println("Part 2: $(fixprogram(code))")
end

end  # module Day08
