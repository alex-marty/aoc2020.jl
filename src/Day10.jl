module Day10

using ..Aoc2020

"""
    prepare(ratings)

Validate and prepare a list of ratings by sorting it, prepending 0 and appending `max + 3`.
"""
function prepare(ratings::Vector{Int})::Vector{Int}
    # Sanity check: unicity of values
    length(ratings) == length(Set(ratings)) || throw(ArgumentError("non-unique ratings"))
    sorted = sort(ratings)
    # Add 0 rating for source and max + 3 for device
    sorted = vcat(0, sorted, sorted[end] + 3)
    # Sanity check: only diffs between 1 and 3
    issubset(diff(sorted), [1, 2, 3]) || throw(ArgumentError("invalid diffs"))
    return sorted
end

"""
    valcount(x)

Count the number of times each unique value appears in an array.
"""
function valcount(x::AbstractArray{T})::Dict{T,Int} where T
    return Dict(v => sum(x .== v) for v in Set(x))
end

"""
    countarrangements(data, maxdiff, cache=Dict())

Count the number of valid arrangements that can be built from the given adapters.
"""
function countarrangements(
    data::Vector{Int}, maxdiff::Int, cache::Dict{Vector{Int},Int} = Dict{Vector{Int},Int}()
)::Int
    len = length(data)
    len == 1 && return 1
    mem = get(cache, data, nothing)
    !isnothing(mem) && return mem
    
    n = 0
    for i in 2:len
        data[i] - data[1] > maxdiff && break
        n += countarrangements(data[i:end], maxdiff, cache)
    end
    cache[data] = n
    return n
end

"""
    run(input_data)

Run the day's puzzle.
"""
@daycli 10 function run(input_data::String)
    ratings = prepare(parseints(input_data))
    dc = valcount(diff(ratings))
    n = dc[1] * dc[3]
    println("Part 1: $n")
    println("Part 2: $(countarrangements(ratings, 3))")
end

end  # module Day10
