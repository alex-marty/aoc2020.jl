module Day07

using ..Aoc2020: @daycli

BagContent = NamedTuple{(:n, :bag), Tuple{Int, String}}

function parsebag(bagline::AbstractString)::Pair{String,Vector{BagContent}}
    bag, contents = split(strip(bagline, '.'), " bags contain ")
    contents == "no other bags" && return bag => []
    contents = map(
        s -> begin
            n, b = match(r"^(\d+) ([a-z ]+) bag(?:s?)$", s).captures
            return (n=parse(Int, n), bag=b)
        end,
        split(contents, ", ")
    )
    return bag => contents
end

function bagcontainers(bagdefs::Dict{String,Vector{BagContent}})::Dict{String,Set{String}}
    containers = Dict(b => Set{String}() for b in keys(bagdefs))
    for (bag, contents) in bagdefs, (n, c) in contents
        push!(containers[c], bag)
    end
    return containers
end

function allparents(bag::String, containers::Dict{String,Set{String}})::Set{String}
    parents = containers[bag]
    isempty(parents) && return Set{String}()
    return union(parents, [allparents(c, containers) for c in parents]...)
end

function nchildren(bag::String, bagdefs::Dict{String,Vector{BagContent}})::Int
    children = bagdefs[bag]
    isempty(children) && return 0
    return sum([c.n * (1 + nchildren(c.bag, bagdefs)) for c in children])
end

@daycli 7 function run(input_data::String)
    bagdefs = Dict(parsebag.(split(input_data, '\n'; keepempty=false)))
    containers = bagcontainers(bagdefs)

    c = allparents("shiny gold", containers)
    println("Part 1: $(length(c)) possible containers")

    n = nchildren("shiny gold", bagdefs)
    println("Part 2: $n children")
end

end  # module Day07
