module Day09

using ..Aoc2020

function findsum(n::Int, factors::Vector{Int})::Union{NTuple{2,Int},Nothing}
    for i in 1:length(factors) - 1, j in i:length(factors)
        if n == factors[i] + factors[j]
            return factors[i], factors[j]
        end
    end
    return nothing
end

function runxmas(data::Vector{Int}, preamble::Int)::Union{Int,Nothing}
    for i = preamble + 1:length(data)
        if isnothing(findsum(data[i], data[i - preamble:i - 1]))
            return data[i]
        end
    end
    return nothing
end

function findsumlist(n::Int, data::Vector{Int})::Union{Vector{Int},Nothing}
    for i in 1:length(data) - 1, j in i:length(data)
        if sum(data[i:j]) == n
            return data[i:j]
        end
    end
    return nothing
end

@daycli 9 function run(input_data::String)
    data = parseints(input_data)
    n = runxmas(data, 25)
    println("Part 1: $n")
    nums = findsumlist(n, data)
    a = minimum(nums)
    b = maximum(nums)
    println("Part 2: $(a + b)")
end

end  # module Day09
