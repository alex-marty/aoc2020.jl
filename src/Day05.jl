module Day05

using ..Aoc2020: @daycli

struct Seat
    row::Int
    column::Int
    id::Int

    function Seat(row, column)
        row >= 0 || throw(ArgumentError("row must be positive"))
        column >= 0 || throw(ArgumentError("column must be positive"))
        return new(row, column, row * 8 + column)
    end
end

_row_conv = Dict('F' => '0', 'B' => '1')
_col_conv = Dict('L' => '0', 'R' => '1')

function Seat(str::AbstractString)::Seat
    length(str) == 10 || throw(ArgumentError("invalid seat string length \"$str\""))
    row = parse(Int, map(c -> _row_conv[c], str[1:7]); base=2)
    col = parse(Int, map(c -> _col_conv[c], str[8:end]); base=2)
    return Seat(row, col)
end

@daycli 5 function run(input_data::String)
    seats = Seat.(split(input_data, '\n'; keepempty=false))

    println("Part 1: id=$(maximum(s.id for s in seats)) ($(length(seats)) seats)")

    ids = Set(s.id for s in seats)
    missing_seats = setdiff(minimum(ids):maximum(ids), ids)
    println("Part 2: missing_seats=$missing_seats")
end

end  # module Day05
