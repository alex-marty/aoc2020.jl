module Day01

using IterTools: subsets

using ..Aoc2020: @daycli

"""
    findcouple(values, total)

Return the first two items in `values` summing up to `total`. If no such items are found,
return `nothing`.
"""
function findcouple(values::Vector{Int}, total::Int)::Union{Tuple{Int, Int}, Nothing}
    length(values) >= 2 || throw(ArgumentError("values must have length at least 2"))
    
    for i = 1:(length(values) - 1), j = (i + 1):length(values)
        if values[i] + values[j] == total
            return values[i], values[j]
        end
    end
    return nothing
end

"""
    subsets_mat(k::Int, n::Int)

Produce subsets of `k` elements chosen in `1:n` (`n` >= `k`), fully materialized as an
array.
"""
function subsets_mat(k::Int, n::Int)::Vector{Vector{Int}}
    # Try materialized, using Channel, and iteration protocol:
    # https://discourse.julialang.org/t/combinations/10026
    k <= n || throw(ArgumentError("n must be >= k"))
    
    state = collect(1:k)
    combs = Vector{Int}[copy(state)]
    idx = k
    while idx > 0
        if state[idx] == n - k + idx
            idx -= 1
        else
            s = state[idx] + 1
            state[idx:end] = s:(s + k - idx)
            push!(combs, copy(state))
            for v = (state[k] + 1):n
                state[k] = v
                push!(combs, copy(state))
            end
            idx = k - 1
        end
    end
    return combs
end

"""
    findtuple(values, k, total)

Return the first set of `k` different items in `values` that sum to `total`, or `nothing` if
not found.
"""
function findtuple(values::Vector{Int}, k::Int, total::Int)::Union{NTuple{k,Int},Nothing}
    k >= 1 || throw(ArgumentError("n_items must be at least 1"))
    length(values) >= k || throw(ArgumentError("values must have length at least n_items"))
    
    for comb = subsets(values, k)
        if sum(comb) == total
            return tuple(comb...)
        end
    end
    return nothing
end

"""
    run(input_data)

Run day 1 puzzle, reading data from `input_data`.
"""
@daycli 1 function run(input_data::String)
    amounts = parse.(Int, split(input_data))

    # Part 1
    couple = findcouple(amounts, 2020)
    p = isnothing(couple) ? nothing : prod(couple)
    println("Part 1: ", couple, ", prod=", p)
    
    # Part 2
    items = findtuple(amounts, 3, 2020)
    println("Part 2: ", items, ", prod=", isnothing(items) ? nothing : prod(items))
end

end  # module Day01
