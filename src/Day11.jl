module Day11

using Base.Iterators

using ..Aoc2020

"""
    adjacentindices(A, I)

Return indices of cells in `A` adjacent to `I`, excluding `I` itself (in column-major order
for efficient iteration). Directions are axes and diagonals.
"""
function adjacentindices(
    A::AbstractArray{T,N} where T, I::CartesianIndex{N}
)::Vector{CartesianIndex{N}} where N
    Ivec = collect(Tuple(I))
    lower = max.(Ivec .- 1, 1)
    upper = min.(Ivec .+ 1, size(A))
    region = product((a:b for (a, b) in zip(lower, upper))...)
    return CartesianIndex.(x for x in region if x != Tuple(I))
end

"""
    visibleindices(A, I)

Return indices of cells in `A` visible from `I`, i.e. that have no seats between them and
`I`, excluding `I` itself (in column-major order for efficient iteration). Directions are
axes and diagonals.
"""
function visibleindices(
    A::AbstractArray{T,N} where T, I::CartesianIndex{N}
)::Vector{CartesianIndex{N}} where N
    n = ndims(A)
    adjregion = product((-1:1 for i in 1:n)...)
    dirs = CartesianIndex.(x for x in adjregion if x != Tuple(zeros(Int, n)))
    
    validregion = CartesianIndices(A)
    visible = CartesianIndex{N}[]
    for dir in dirs
        x = I
        while true
            x += dir
            x in validregion || break
            if A[x] != '.'
                push!(visible, x)
                break
            end
        end
    end
    return visible
end

"""
    updateseats(grid, seatfunc, k)

Return the updated seating area `grid`, using the visibility function `seatfunc` and a
minimum number of occupied seats `k` for the emptying rule.
"""
function updateseats(grid::Matrix{Char}, seatfunc::Function, k::Int)::Matrix{Char}
    newgrid = copy(grid)
    for I in CartesianIndices(grid)
        seat = grid[I]
        seat == '.' && continue
        adj = grid[seatfunc(grid, I)]
        if seat == 'L' && sum(adj .== '#') == 0
            newgrid[I] = '#'
        elseif seat == '#' && sum(adj .== '#') >= k
            newgrid[I] = 'L'
        end
    end
    return newgrid
end

"""
    seatsimulation(grid, seatfunc, k)

Run the simulation of the seating area `grid`, starting with an empty area, until
stabilization, and return the number of occupied seats.
Use the visibility function `seatfunc` and a minimum number of occupied seats `k` for the
emptying rule.
"""
function seatsimulation(grid::Matrix{Char}, seatfunc::Function, k::Int)::Int
    x = y = grid
    while true
        y = updateseats(x, seatfunc, k)
        y == x && break
        x = y
    end
    return sum(x .== '#')
end

seatsimulation_adjacent(grid::Matrix{Char})::Int = seatsimulation(grid, adjacentindices, 4)
seatsimulation_visible(grid::Matrix{Char})::Int = seatsimulation(grid, visibleindices, 5)

@daycli 11 function run(input_data::String)
    grid = parsegrid(input_data)
    println("Part 1: $(seatsimulation_adjacent(grid)) seats occupied / $(length(grid))")
    println("Part 2: $(seatsimulation_visible(grid)) seats occupied / $(length(grid))")
end

end  # module Day11
