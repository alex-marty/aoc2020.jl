module Day06

using ..Aoc2020: @daycli

function parseanswers(input::AbstractString)::Vector{Vector{String}}
    groups = split(input, "\n\n"; keepempty=false)
    # If we don't convert to String, we get SubString{String} as output and consumers' input
    # types are trickier to get right, even with AbstractString. Investigate this.
    # return split.(groups, "\n"; keepempty=false)
    return [string.(split(g, "\n"; keepempty=false)) for g in groups]
end

function groupunique(answers::Vector{String})::Set{Char}
    return Set(string(answers...))  # Could also do a union() of Sets
end

function groupall(answers::Vector{String})::Set{Char}
    return intersect(Set.(answers)...)
end

@daycli 6 function run(input_data::String)
    group_answers = parseanswers(input_data)

    unique_answers = groupunique.(group_answers)
    println("Part 1: $(sum(length.(unique_answers)))")

    all_answers = groupall.(group_answers)
    println("Part 2: $(sum(length.(all_answers)))")
end

# @daycli 6 run

end  # module Day06
