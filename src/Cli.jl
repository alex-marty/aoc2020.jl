"""Terminal commands utils"""
module Cli

using Base: @__doc__
using Base.Meta: esc, isexpr

export Command, CommandGroup, run, @subcommand

struct Command
    name::String
    callback::Function
end

function Command(callback::Function)::Command
    return Command(string(callback), callback)
end

struct CommandGroup
    name::String
    commands::Vector{Command}
    _name_mapping::Dict{String,Command}
    
    function CommandGroup(name::String, commands::Vector{Command} = Command[])
        name_mapping::Dict{String,Command} = Dict(cmd.name => cmd for cmd in commands)
        if length(name_mapping) != length(commands)
            throw(ArgumentError("duplicated command names"))
        end
        return new(name, commands, name_mapping)
    end
end

function Base.push!(cmdgroup::CommandGroup, cmd::Command)
    if cmd.name in keys(cmdgroup._name_mapping)
        throw(ArgumentError("command name \"$(cmd.name)\" already in $cmdgroup"))
    end
    Base.push!(cmdgroup.commands, cmd)
    cmdgroup._name_mapping[cmd.name] = cmd
end

Base.getindex(cmdgroup::CommandGroup, cmdname::String) = cmdgroup._name_mapping[cmdname]
Base.getindex(cmdgroup::CommandGroup, i::Int) = cmdgroup.commands[i]

function run(group::CommandGroup, args::Vector{String})
    if isempty(args)
        println("usage: julia $PROGRAM_FILE command\n\nAvailable commands:")
        cmd_names = sort(map(cmd -> cmd.name, group.commands))
        println(join([string("  ", c) for c in cmd_names], "\n"))
    else
        command = get(group._name_mapping, args[1], nothing)
        isnothing(command) && throw(ArgumentError("invalid command \"$(args[1])\""))
        run(command, args[2:end])
    end
end

function run(command::Command, args::Vector{String})
    command.callback(args...)
end

macro subcommand(group::Symbol, name::String, func::Symbol)
    return quote
        cmd = Command($name, $(esc(func)))
        push!($(esc(group)), cmd)
        return cmd
    end
end

macro subcommand(group::Symbol, func::Symbol)
    return esc(:(@subcommand $group $(string(func)) $func))
end

function funcsymbol(funcdef::Expr)
    if !isexpr(funcdef, :function)
        throw(ArgumentError("expression must be a function definition"))
    end
    return funcdef.args[1].args[1]
end

macro subcommand(group::Symbol, name::String, funcdef::Expr)
    show(group)
    show(name)
    show(funcdef)
    funcsym = funcsymbol(funcdef)
    return quote
        @__doc__ $(esc(funcdef))
        cmd = Command($name, $(esc(funcsym)))
        push!($(esc(group)), cmd)
        return cmd
    end
end

macro subcommand(group::Symbol, funcdef::Expr)
    funcsym = funcsymbol(funcdef)
    return @__doc__ esc(:(@subcommand $group $(string(funcsym)) $funcdef))
end

end  # module Cli
