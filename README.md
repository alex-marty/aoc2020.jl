# Aoc2020.jl

[![Build Status](https://gitlab.com/alex-marty/Aoc2020.jl/badges/master/pipeline.svg)](https://gitlab.com/alex-marty/Aoc2020.jl/pipelines)
[![Coverage](https://gitlab.com/alex-marty/Aoc2020.jl/badges/master/coverage.svg)](https://gitlab.com/alex-marty/Aoc2020.jl/commits/master)


Using [Advent of Code 2020](https://adventofcode.com/2020/) as an exercise to learn Julia.
